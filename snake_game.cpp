﻿#include <windows.h>
#include "src/game/Config.h"
#include "src/game/GameCore.h"


void customizeOutput()
{
	HANDLE stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	// ======================================== CHANGE FONT AND SIZE
	CONSOLE_FONT_INFOEX cfi;
	cfi.cbSize = sizeof(cfi);
	cfi.nFont = 0;
	cfi.dwFontSize.X = 8; // Width of each character in the font
	cfi.dwFontSize.Y = 16; // Height
	cfi.FontFamily = FF_DONTCARE;
	cfi.FontWeight = FW_NORMAL;
	wcscpy_s(cfi.FaceName, L"Consolas"); // Choose your font
	SetCurrentConsoleFontEx(stdHandle, FALSE, &cfi);

	// ======================================== CHANGE FONT COLOR
	SetConsoleTextAttribute(stdHandle, 10);
	// 10 is id of theme, stands for: green text and black background

	// ======================================== SET SCREEN SIZE
	HWND console = GetConsoleWindow();
	
	RECT rect;
	GetWindowRect(console, &rect); //stores the console's current dimensions
	MoveWindow(console, rect.left, rect.top, 800, 560, TRUE);
	// Note: we don't actually move window, but resize it.

	// ======================================== HIDE CMD CURSOR
	CONSOLE_CURSOR_INFO cursorInfo;

	GetConsoleCursorInfo(stdHandle, &cursorInfo);
	cursorInfo.bVisible = false; // set the cursor visibility
	SetConsoleCursorInfo(stdHandle, &cursorInfo);
}

int main()
{
	customizeOutput();

	GameCore game(Config::msPerFrame);
	game.setup();
	game.startGameLoop();
}
