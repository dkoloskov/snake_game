#include "GameCore.h"
#include "states/start_screen/StartScreenState.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
GameCore::GameCore(double loopTime)
	: AbstractGameLoop(loopTime)
{
}

void GameCore::setup()
{
	input = std::make_unique<KeyboardInput>();
}

void GameCore::startGameLoop()
{
	std::function<void(AbstractGameState*)> func = std::bind(&GameCore::changeState, this, std::placeholders::_1);
	changeState(new StartScreenState(func));

	AbstractGameLoop::startGameLoop();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
void GameCore::processInput()
{
	std::lock_guard<std::mutex> guard(input->pressButtonMutex);
	currentState->processInput(input->getPressedButton());
}

void GameCore::update(double delta)
{
	currentState->update(delta);
}

void GameCore::render()
{
	currentState->render();
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void GameCore::changeState(AbstractGameState *newState)
{
	// Call destructor of current state, and set new state
	currentState.reset(newState);
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
