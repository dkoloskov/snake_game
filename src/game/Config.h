#pragma once
#include <cstdint>


/**
 * Game configurations/settings
 */
struct Config
{
	static const uint8_t fps = 60; // frames per second (game update rate)
	// ======================================== IMPORTANT
	// system("cls"); // clear screen
	// Note: system("cls") eats ~25-35 ms (on 4Ghz CPU),
	// while with 60 FPS screen rendered each 16.7 ms,
	// and with 30FPS screen renderend each 33.3 ms
	// ... THAT IS WHY FPS SET SO LOW

	inline static const double msPerFrame = 1000.0 / fps;

	// GAME FIELD
	static const uint8_t gameFieldWidth = 20;
	static const uint8_t gameFieldHeight = 20;

	// SNAKE
	inline static const float snakeStartSpeed = 3.f / 1000.f; // pixel-char per millisecond
	inline static const float snakeSpeedIncrease = snakeStartSpeed * 0.05; // +5% speed with each new food/piece consumed
};
