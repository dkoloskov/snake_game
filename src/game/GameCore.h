#pragma once
#include "../architecture/game_loop/AbstractGameLoop.h"
#include <memory>
#include "input/KeyboardInput.h"
#include "states/AbstractGameState.h"


class GameCore : public AbstractGameLoop
{
public:
	GameCore(double loopTime);
	
	virtual void setup() override;
	virtual void startGameLoop() override;
	void changeState(AbstractGameState* newState);
protected:
	virtual void processInput() override;
	virtual void update(double delta) override;
	virtual void render() override;

private:
	std::unique_ptr<KeyboardInput> input;
	std::unique_ptr<AbstractGameState> currentState;
};
