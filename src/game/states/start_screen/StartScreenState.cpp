#include "StartScreenState.h"
#include <iostream>
#include "../gameplay/GameplayState.h"

using std::cout, std::endl;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
StartScreenState::StartScreenState(std::function<void(AbstractGameState *)> &changeState)
	: AbstractGameState(changeState)
{
}

void StartScreenState::processInput(ButtonsEnum input)
{
	switch (input)
	{
	case ButtonsEnum::ENTER:
		changeState(new GameplayState(changeState));
		break;
	case ButtonsEnum::A:
		// TODO: implement autoplay
		break;
	}
}

void StartScreenState::update(double delta)
{
	// Nothing to update in this state
}

void StartScreenState::render()
{
	// In this state we don't need to update screen constantly
	if (outputDisplayed)
		return;
	outputDisplayed = true;

	//
	system("cls"); // clear screen
	cout << endl;
	cout << "	SNAKE GAME" << endl;
	cout << endl;
	cout << "	Keyboard controls:" << endl;
	cout << "		up arrow - up" << endl;
	cout << "		down arrow - down" << endl;
	cout << "		left arrow - left" << endl;
	cout << "		right arrow - right" << endl;
	cout << "		space - pause / unpause" << endl;
	cout << endl;
	cout << "	To start game press \"ENTER\"" << endl;
	//cout << "	To start game in AUTO mode press \"A\" (NOT IMPLEMENTED)" << endl;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
