#pragma once
#include "../AbstractGameState.h"


class StartScreenState : public AbstractGameState
{
public:
	StartScreenState(std::function<void(AbstractGameState*)> &changeState);

	virtual void processInput(ButtonsEnum input) override;
	virtual void update(double delta) override;
	virtual void render() override;

private:
	bool outputDisplayed = false;
};
