#pragma once


struct ViewChar
{
	static const char wall = '#';

	static const char snakeBody = 'o';
	static const char snakeHead = 'O';
	
	static const char fruit = '0';

	static const char blank = ' ';
	static const char end = '\n';
};