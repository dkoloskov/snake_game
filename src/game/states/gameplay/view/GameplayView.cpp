#include "GameplayView.h"
#include <iostream>
#include "ViewChar.h"

using std::cout, std::endl;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
GameplayView::GameplayView(std::shared_ptr<GameplayData> data)
	: data(data)
{
	// ======================================== Calculate view size
	width = (data->gameFieldWidth * 2) - 1 + textBlockSize + 3 + (gfMarginH * 2) + tbMarginH;
	// "(data->gameFieldWidth * 2) - 1" - HACK to fix: fast vertical movement and slow horizontal
	// "+ 3" - 2 (2x1 walls) + 1 (\n char)
	// "gfMarginH * 2" - "*2" because two sides

	height = data->gameFieldHeight + 2 + totalMarginV;
	// "+ 2" - 2 (2x1 walls)

	// ======================================== Fill buffer with default chars
	drawBlankScreen();
	drawWalls();
}

void GameplayView::update()
{
	draw();
	render();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void GameplayView::draw()
{
	drawGameField();
	drawFruit();
	drawSnake();

	//
	drawScore();
	drawPause();
	drawGameOver();
}

void GameplayView::render()
{
	// ======================================== IMPORTANT
	// system("cls"); // clear screen
	// Note: system("cls") eats ~25-35 ms (on 4Ghz CPU),
	// while with 60 FPS screen rendered each 16.7 ms,
	// and with 30FPS screen renderend 33.3 ms
	// 
	// So, I use big "GameplayView::gfMarginV" as HACK

	cout << buffer;
}

// ======================================== DRAW
void GameplayView::drawBlankScreen()
{
	buffer = "";
	buffer.resize(width * height, ' ');

	//
	uint16_t shift = 0;
	for (uint8_t i = 0; i < height; i++)
	{
		// fill line with blank chars
		shift = i * width;
		std::fill_n(buffer.begin() + shift, width - 1, ViewChar::blank);

		// put \n at the end of new line
		shift += width - 1;
		buffer[shift] = ViewChar::end;
	}
}

void GameplayView::drawWalls()
{
	// Draw top wall
	uint16_t shift = gfMarginV * width + gfMarginH;
	std::fill_n(buffer.begin() + shift, data->gameFieldWidth * 2 + 1, ViewChar::wall);

	// Draw side walls
	for (uint8_t i = gfMarginV + 1; i < data->gameFieldHeight + gfMarginV + 1; i++)
	{
		// wall at the begining
		shift = i * width + gfMarginH;
		buffer[shift] = ViewChar::wall;

		// wall at the end
		shift += data->gameFieldWidth * 2;
		buffer[shift] = ViewChar::wall;
	}

	// Draw bottom wall
	shift = (data->gameFieldHeight + gfMarginV + 1) * width + gfMarginH;
	std::fill_n(buffer.begin() + shift, data->gameFieldWidth * 2 + 1, ViewChar::wall);
}

// ======================================== DRAW CONSTANTLY
void GameplayView::drawGameField()
{
	uint16_t shift;
	for (uint8_t i = gfMarginV + 1; i < data->gameFieldHeight + 1 + gfMarginV; i++)
	{
		// fill line with blank chars
		shift = i * width + gfMarginH + 1;
		std::fill_n(buffer.begin() + shift, data->gameFieldWidth * 2 - 1, ViewChar::blank);
	}
}

void GameplayView::drawSnake()
{
	SnakeNode *head = data->snakeData->getHead();

	// draw body
	SnakeNode *sNode = head->getSnakeChild();
	while (sNode != nullptr)
	{
		draw2D(sNode->point, ViewChar::snakeBody);
		sNode = sNode->getSnakeChild();
	}

	// draw head
	draw2D(head->point, ViewChar::snakeHead);
	// Note: we draw it here, so body wouldn't be drawn over head (when eat first fruit)
}

void GameplayView::drawFruit()
{
	draw2D(data->fruitPoint, ViewChar::fruit);
}

//
void GameplayView::drawScore()
{
	uint16_t shift = tbMarginV * width + (data->gameFieldWidth * 2) - 1 + totalMarginH + 2;
	std::string text = "SCORE: " + std::to_string(data->getScore());

	buffer.replace(buffer.begin() + shift, buffer.begin() + shift + text.length(), text);
}

void GameplayView::drawPause()
{
	uint16_t shift = (tbMarginV + 2) * width + (data->gameFieldWidth * 2) - 1 + totalMarginH + 2;
	std::string text = data->isPaused() ? "PAUSED" : "      ";

	buffer.replace(buffer.begin() + shift, buffer.begin() + shift + text.length(), text);
}

void GameplayView::drawGameOver()
{
	if (!data->isGameOver())
		return;

	uint16_t shift = (tbMarginV + 2) * width + (data->gameFieldWidth * 2) - 1 + totalMarginH + 2;
	std::string text = "GAME OVER";
	buffer.replace(buffer.begin() + shift, buffer.begin() + shift + text.length(), text);

	shift += width;
	text = "Press \"ENTER\" to restart";
	buffer.replace(buffer.begin() + shift, buffer.begin() + shift + text.length(), text);
}

//
void GameplayView::draw2D(Point2D point, char viewChar)
{
	uint16_t shift = width * (point.y + gfMarginV + 1) + (point.x * 2 + 1) + gfMarginH;
	buffer[shift] = viewChar;
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
GameplayView::~GameplayView()
{
	data = nullptr;
}
