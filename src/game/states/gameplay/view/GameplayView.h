#pragma once
#include <string>
#include <memory>
#include "../GameplayData.h"
#include "../Point2D.h"


class GameplayView
{
public:
	GameplayView(std::shared_ptr<GameplayData> data);
	virtual ~GameplayView();

	void update();

private:
	std::shared_ptr<GameplayData> data;
	std::string buffer;

	// View size
	uint8_t width; // in chars-pixels
	uint8_t height; // in char-pixels
	// Game field style (in char-pixels)
	const uint8_t gfMarginV = 10; // margin top <---------- HACK!
	// HACK above: because of VERY slow "system("cls");" I use big margin from top to make ...
	//			   ... cmd window look updated. Tested on 2 devices, looks not very bad.
	//			   Fix side effect: badly controlled vertical-align of text in the window.
	
	const uint8_t gfMarginH = 4; // margin left/right
	// Text block style ("Score:XXXX") (in char-pixels)
	const uint8_t textBlockSize = 24;
	const uint8_t tbMarginV = gfMarginV; // margin top
	const uint8_t tbMarginH = 0; // margin left
	// totalMarginV not calculated since game field and text block aligned horizontally
	const uint8_t totalMarginV = (gfMarginV > tbMarginV) ? gfMarginV : tbMarginV;
	// totalMarginV not calculated since game field and text block aligned horizontally
	const uint8_t totalMarginH = (gfMarginH*2) + tbMarginH;
	// Note: I know that "style" above probably redundant, but I started this "margin thing" and
	//		 wasn't able to stop ...
	// Note 2: I know that this "style" can be improved, by moving all the "aligning logic" to
	//		   some AbstractViewBox and wrap all the view blocks with this but I don't want to
	//		   spend additional time for this anymore.

	void draw();
	void render(); // Not a real render, just a print to cmd

	// ======================================== DRAW ONCE
	void drawBlankScreen(); // fill buffer with blank chars
	void drawWalls();

	// ======================================== DRAW CONSTANTLY
	void drawGameField();
	void drawSnake();
	void drawFruit();
	//
	void drawScore();
	void drawPause();
	void drawGameOver();

	// Sintax sugar. Converting 2D to 1D array (buffer), for easier draw() of snake and fruit
	void draw2D(Point2D point, char viewChar);
};
