#pragma once
#include "../AbstractGameState.h"
#include <memory>
#include "GameplayData.h"
#include "view/GameplayView.h"


class GameplayState: public AbstractGameState
{
	
public:
	GameplayState(std::function<void(AbstractGameState*)> &changeState);
	
	virtual void processInput(ButtonsEnum input) override;
	virtual void update(double delta) override;
	virtual void render() override;

private:
	std::shared_ptr<GameplayData> data;
	std::unique_ptr<GameplayView> view;
};

