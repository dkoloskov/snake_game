#include "SnakeNode.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
SnakeNode::SnakeNode(Point2D point)
	: point(point)
{
}

//
void SnakeNode::move(Point2D newPoint)
{
	if (hitTestParent(newPoint))
	{
		return;
	}

	Point2D previousPoint = point;
	point = newPoint;

	SnakeNode *childSnake = dynamic_cast<SnakeNode*>(child);
	if (childSnake)
	{
		childSnake->move(previousPoint);
	}
}

//
bool SnakeNode::hitTest(Point2D point)
{
	if (point.x == this->point.x && point.y == this->point.y)
	{
		return true;
	}
	else if (child != nullptr)
	{
		return getSnakeChild()->hitTest(point);
	}

	return false;
}

//
SnakeNode* SnakeNode::getSnakeChild()
{
	if (child == nullptr)
	{
		return nullptr;
	}

	SnakeNode *snakeChild = dynamic_cast<SnakeNode*>(child);
	return snakeChild;
}

SnakeNode* SnakeNode::getTail()
{
	if (child == nullptr)
	{
		return this;
	}

	return getSnakeChild()->getTail();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
bool SnakeNode::hitTestParent(const Point2D &newPoint)
{
	// if current node is head
	if (parent == nullptr)
	{
		return false;
	}

	SnakeNode *snakeParent = dynamic_cast<SnakeNode*>(parent);
	if (newPoint.x == snakeParent->point.x && newPoint.y == snakeParent->point.y)
	{
		return true;
	}

	return false;
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
SnakeNode::~SnakeNode()
{
}
