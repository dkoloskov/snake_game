#pragma once
#include "SnakeNode.h"
#include "../Point2D.h"


class SnakeData
{
public:
	SnakeData(Point2D startPoint);
	virtual ~SnakeData();

	/**
	 * @return - true, when position was updated
	 */
	bool updatePosition(double delta);

	//
	void setMoveDirection(Point2D newDirection);

	bool tryEatFruit(Point2D fruitPos);
	bool hitTest(Point2D point);
	bool headHitTestBody();
	bool headHitTestWall(uint8_t gameFieldWidth, uint8_t gameFieldHeight);

	//
	SnakeNode* getHead();

	void setDead();
	bool isDead();

private:
	SnakeNode *head;
	Point2D moveDirection;

	float speed; // pixel-char per millisecond
	float moveGauge = 0.f; // when filled, snake will move

	bool dead = false; // switch when head hit it's own body or wall

	//
	void increaseSize();
	void increaseSpeed();
	
	//
	void setRandomMoveDirection();
};
