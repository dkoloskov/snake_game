#include "SnakeData.h"
#include "../MoveDirection.h"
#include "../../../../utils/Random.h"
#include "../../../Config.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
SnakeData::SnakeData(Point2D startPoint)
{
	head = new SnakeNode(startPoint);

	speed = Config::snakeStartSpeed;
	setRandomMoveDirection();
}

bool SnakeData::updatePosition(double delta)
{
	moveGauge += speed * delta;
	if (moveGauge >= 1)
	{
		moveGauge -= 1;

		Point2D newHeadPoint = {head->point.x + moveDirection.x, head->point.y + moveDirection.y};
		head->move(newHeadPoint);

		return true;
	}

	return false;
}

//
void SnakeData::setMoveDirection(Point2D newDirection)
{
	// Check if we want to turn head to body direction, WE CAN'T!
	SnakeNode *firstBodyNode = head->getSnakeChild();
	if (firstBodyNode != nullptr)
	{
		int32_t newX = head->point.x + newDirection.x;
		int32_t newY = head->point.y + newDirection.y;

		if (firstBodyNode->point.x == newX && firstBodyNode->point.y == newY)
		{
			return;
		}
	}

	moveDirection = newDirection;
}

bool SnakeData::tryEatFruit(Point2D fruitPos)
{
	if (head->point.x == fruitPos.x && head->point.y == fruitPos.y)
	{
		increaseSize();
		increaseSpeed();

		return true;
	}
	return false;
}

bool SnakeData::hitTest(Point2D point)
{
	return head->hitTest(point);
}

bool SnakeData::headHitTestBody()
{
	SnakeNode *bodyNode = head->getSnakeChild();
	if (bodyNode == nullptr)
	{
		return false;
	}

	return bodyNode->hitTest(head->point);
}

bool SnakeData::headHitTestWall(uint8_t gameFieldWidth, uint8_t gameFieldHeight)
{
	if (head->point.x < 0 || head->point.y < 0 || head->point.x >= gameFieldWidth || head->point.y >= gameFieldHeight)
		return true;

	return false;
}

//
SnakeNode* SnakeData::getHead()
{
	return head;
}

void SnakeData::setDead()
{
	dead = true;
	speed = 0;
}

bool SnakeData::isDead()
{
	return dead;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void SnakeData::increaseSize()
{
	SnakeNode *tail = head->getTail();

	SnakeNode *newTail = new SnakeNode(tail->point);
	tail->addChild(newTail);
	// We add new tail to old tail point and during next move:
	//	- old tail will move to new place
	//	- new tail will stay at one place for one move
	//
	// There is another approach with immediately increasing the tail, but it's more complicated ...
	// ... because of turns. I will need to save previous SnakeNode points, and I don't want ...
	// ... overcomplicate (more) game mechanics.
}

void SnakeData::increaseSpeed()
{
	speed += Config::snakeSpeedIncrease;
}

//
void SnakeData::setRandomMoveDirection()
{
	int8_t randomNumber = Random::get(1, 4);
	switch (randomNumber)
	{
	case 1: moveDirection = MoveDirection::UP;
		break;
	case 2: moveDirection = MoveDirection::DOWN;
		break;
	case 3: moveDirection = MoveDirection::LEFT;
		break;
	case 4: moveDirection = MoveDirection::RIGHT;
		break;
	}
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
SnakeData::~SnakeData()
{
	delete head;
}
