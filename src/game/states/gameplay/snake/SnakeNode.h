#pragma once
#include "../../../../architecture/node/Node.h"
#include "../Point2D.h"


class SnakeNode : public Node
{
public:
	SnakeNode(Point2D point);
	virtual ~SnakeNode();

	//
	Point2D point;
	void move(Point2D newPoint);

	//
	bool hitTest(Point2D point);
	
	//
	SnakeNode* getSnakeChild();
	SnakeNode* getTail();

private:
	// Case when the new tail has the same position as old tail and old tail already ...
	// ... taken a new position
	bool hitTestParent(const Point2D& newPoint);
};
