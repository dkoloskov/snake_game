#include "GameplayState.h"
#include "MoveDirection.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
GameplayState::GameplayState(std::function<void(AbstractGameState *)> &changeState)
	: AbstractGameState(changeState)
{
	data = std::make_shared<GameplayData>();
	view = std::make_unique<GameplayView>(data);
}

void GameplayState::processInput(ButtonsEnum input)
{
	switch (input)
	{
	case ButtonsEnum::UP:
		// Created states but ended up doing this... Not cool.
		if(!data->isGameOver() && !data->isPaused())
			data->snakeData->setMoveDirection(MoveDirection::UP);
		break;
	case ButtonsEnum::DOWN:
		if (!data->isGameOver() && !data->isPaused())
			data->snakeData->setMoveDirection(MoveDirection::DOWN);
		break;
	case ButtonsEnum::LEFT:
		if (!data->isGameOver() && !data->isPaused())
			data->snakeData->setMoveDirection(MoveDirection::LEFT);
		break;
	case ButtonsEnum::RIGHT:
		if (!data->isGameOver() && !data->isPaused())
			data->snakeData->setMoveDirection(MoveDirection::RIGHT);
		break;
	case ButtonsEnum::SPACE:
		if (!data->isGameOver())
   			data->switchPause();
	case ButtonsEnum::ENTER:
		if (data->isGameOver())
			changeState(new GameplayState(changeState));
			// Note: we change current state to NEW current state
		break;
	}
}

void GameplayState::update(double delta)
{
	if(!data->isPaused())
		data->update(delta);
}

void GameplayState::render()
{
	view->update();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
