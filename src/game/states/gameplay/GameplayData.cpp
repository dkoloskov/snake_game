#include "GameplayData.h"
#include "../../../utils/Random.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
GameplayData::GameplayData()
{
	snakeData = std::make_unique<SnakeData>(getFieldCenter());

	spawnFruit();
}

void GameplayData::update(double delta)
{
	bool snakePosUpdated = snakeData->updatePosition(delta);
	if (snakePosUpdated)
	{
		if (snakeData->headHitTestWall(gameFieldWidth, gameFieldHeight) || snakeData->headHitTestBody())
		{
			snakeData->setDead();
			gameOver = true;
			return;
		}

		bool fruitEaten = snakeData->tryEatFruit(fruitPoint);
		if (fruitEaten)
		{
			score++;
			spawnFruit();
		}
	}
}

void GameplayData::spawnFruit()
{
	Point2D newFruitPos;
	bool fruitHitSnakeBody = false;
	do
	{
		newFruitPos = {Random::get(0, gameFieldWidth - 1), Random::get(0, gameFieldHeight - 1)};
		fruitHitSnakeBody = snakeData->hitTest(newFruitPos);
	}
	while (fruitHitSnakeBody);
	fruitPoint = newFruitPos;

	// Note: I know this is BAD approach, better will be to:
	//	1. Calculate array of free cells on the field, and choose random position from there.
	//	   Motivation not to do it: Refactoring may be required, I don't have time for this now.
	//	2. Do the same, but also call method asynchronously after few attempts. Also BAD ...
	//	  ... approach,	but really faster to implement. And it will also prevent game freeze.
}

void GameplayData::switchPause()
{
	pause = !pause;
}

//
uint8_t GameplayData::getScore()
{
	return score;
}

bool GameplayData::isPaused()
{
	return pause;
}

bool GameplayData::isGameOver()
{
	return gameOver;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
Point2D GameplayData::getFieldCenter()
{
	Point2D center = {gameFieldWidth / 2, gameFieldHeight / 2};
	return center;
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
GameplayData::~GameplayData()
{
}
