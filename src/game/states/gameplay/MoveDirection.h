#pragma once
#include "Point2D.h"


struct MoveDirection
{
	// Up goes down, since game field drawn from top to bottom
	inline static const Point2D UP = { 0, -1 };
	inline static const Point2D DOWN = { 0, 1 };
	inline static const Point2D LEFT = { -1, 0 };
	inline static const Point2D RIGHT = { 1, 0 };
};
