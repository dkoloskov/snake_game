#pragma once
#include <memory>
#include "Point2D.h"
#include "snake/SnakeData.h"
#include "../../Config.h"


class GameplayData
{
public:
	GameplayData();
	virtual ~GameplayData();

	const uint8_t gameFieldWidth = Config::gameFieldWidth;
	const uint8_t gameFieldHeight = Config::gameFieldHeight;
	std::unique_ptr<SnakeData> snakeData; // Note: I know that this must be encapsulated, but... I'm lazy now

	//
	Point2D fruitPoint;

	//
	void update(double delta);

	void spawnFruit();
	void switchPause();
	
	//
	uint8_t getScore(); // no need to be constant since we return by value
	bool isPaused();
	bool isGameOver();

private:
	uint8_t score = 0;
	bool gameOver = false;
	bool pause = false;
	// Note: at first I wanted to create "GameOverState", but then decided that leave game ...
	// ... screen visible after loose looks better.

	// Requred to set start position of the snake
	Point2D getFieldCenter();
};
