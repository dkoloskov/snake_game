#pragma once
#include "../input/ButtonsEnum.h"
#include <functional>


/**
 * State design pattern
 */
class AbstractGameState
{
public:
	explicit AbstractGameState(std::function<void(AbstractGameState*)> &changeState);

	virtual void processInput(ButtonsEnum input) = 0;
	virtual void update(double delta) = 0;
	virtual void render() = 0;

protected:
	std::function<void(AbstractGameState *)> changeState;
};
