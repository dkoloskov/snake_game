#pragma once

// Class specific only for current game (don't want to handle all the keyboard keys)
enum class ButtonsEnum
{
	NONE = 0,
	//
	UP,		// move up
	DOWN,	// move down
	LEFT,	// move left
	RIGHT,	// move right
	//
	ENTER,	// start game
	SPACE,	// pause / unpause
	A		// auto play mode
};
