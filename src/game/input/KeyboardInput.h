#pragma once
#include "ButtonsEnum.h"
#include <thread>
#include <memory>
#include <unordered_map>
#include <mutex>
#include "../../utils/Timer.h"


/**
 * Current class saves last pressed keyboard button
 * 
 * This class purpose is mostly to server as example of work with asynchronous thread,
 * but I think same results can be achieved in 1 thread calling GetAsyncKeyState();
 */
class KeyboardInput
{
public:
	KeyboardInput();
	virtual ~KeyboardInput();

	std::mutex pressButtonMutex;
	ButtonsEnum getPressedButton();
	
private:
	std::unique_ptr<std::thread> keyboardThread;
	bool runKeyboardThread = false;
	uint32_t waitTime = 20; // How often we will check for input (in Milliseconds)
	
	ButtonsEnum pressedButton = ButtonsEnum::NONE;
	void pressButtonHandle();
	
	// ======================================== BUTTONS LOCKER
	// Some buttons should be allowed to spam, while some should be limited.
	// Note: found that problem when faced SPACE=>Game Pause spam 5-10 times in row
	Timer timer; // to handle passed time after button press
	std::unordered_map<ButtonsEnum, TimePoint_t> buttonsLocker;

	const double lockTime = 300.0; // milliseconds (should be enough)

	void pressLockableButton(const ButtonsEnum& button);
	
	bool buttonIsLocked(const ButtonsEnum& button);
	void lockButton(const ButtonsEnum& button);
	bool tryToUnlockButton(const ButtonsEnum& button);
};

