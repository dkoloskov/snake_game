#include "KeyboardInput.h"
#include <windows.h>
#include <thread>


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
KeyboardInput::KeyboardInput()
{
	runKeyboardThread = true;
	keyboardThread.reset(new std::thread(&KeyboardInput::pressButtonHandle, this));
}

ButtonsEnum KeyboardInput::getPressedButton()
{
	ButtonsEnum temp = pressedButton;
	pressedButton = ButtonsEnum::NONE;
	// set pressedButton to none required, to block unwanted button press events
	// While this thread asleep, main thread can request for pressed button few times,
	// so we make sure that main thread will receive only 1 event.

	return temp;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void KeyboardInput::pressButtonHandle()
{
	while (runKeyboardThread)
	{
		std::unique_lock<std::mutex> uLock(pressButtonMutex);
		pressedButton = ButtonsEnum::NONE;

		// Only one key can be handled at a time (since we don't required more)
		if (GetAsyncKeyState(VK_UP) < 0)
		{
			pressedButton = ButtonsEnum::UP;
		}
		else if (GetAsyncKeyState(VK_DOWN) < 0)
		{
			pressedButton = ButtonsEnum::DOWN;
		}
		else if (GetAsyncKeyState(VK_LEFT) < 0)
		{
			pressedButton = ButtonsEnum::LEFT;
		}
		else if (GetAsyncKeyState(VK_RIGHT) < 0)
		{
			pressedButton = ButtonsEnum::RIGHT;
		}
		else if (GetAsyncKeyState(VK_RETURN) < 0)
		{
			pressLockableButton(ButtonsEnum::ENTER);
		}
		else if (GetAsyncKeyState(VK_SPACE) < 0)
		{
			pressLockableButton(ButtonsEnum::SPACE);
		}
		else if (GetAsyncKeyState('A') < 0)
		{
			pressedButton = ButtonsEnum::A;
		}
		uLock.unlock();

		std::this_thread::sleep_for(std::chrono::milliseconds(waitTime));
	}
}

//
void KeyboardInput::pressLockableButton(const ButtonsEnum &button)
{
	if (!buttonIsLocked(button) || tryToUnlockButton(button))
	{
		pressedButton = button;
		lockButton(pressedButton);
	}
}

bool KeyboardInput::buttonIsLocked(const ButtonsEnum &button)
{
	auto got = buttonsLocker.find(button);
	if (got != buttonsLocker.end())
	{
		return true;
	}

	return false;
}

void KeyboardInput::lockButton(const ButtonsEnum &button)
{
	if (buttonIsLocked(button))
	{
		// If already locked
		return;
	}

	// Create lock
	buttonsLocker.insert({button, {timer.reset()}});
}

bool KeyboardInput::tryToUnlockButton(const ButtonsEnum &button)
{
	auto got = buttonsLocker.find(button);
	if (got == buttonsLocker.end())
	{
		// If NOT locked
		return true;
	}

	// Release lock (remove item from map)
	double elapsed = timer.elapsed(got->second);
	if (elapsed >= lockTime)
	{
		buttonsLocker.erase(button);
		return true;
	}

	return false;
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
KeyboardInput::~KeyboardInput()
{
	runKeyboardThread = false;
	if (keyboardThread->joinable())
	{
		keyboardThread->join();
	}
}
