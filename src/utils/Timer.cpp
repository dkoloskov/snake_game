#include "Timer.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
Timer::Timer()
{
	reset();
}

TimePoint_t Timer::reset()
{
	startPoint = Clock_t::now();
	return startPoint;
}

double Timer::elapsed() const
{
	return std::chrono::duration_cast<Milliseconds_t>(Clock_t::now() - startPoint).count();
}

double Timer::elapsed(const TimePoint_t &timePoint) const
{
	return std::chrono::duration_cast<Milliseconds_t>(Clock_t::now() - timePoint).count();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
