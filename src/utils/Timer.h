#pragma once
#include <chrono>

using Clock_t = std::chrono::high_resolution_clock;
using TimePoint_t = std::chrono::time_point<Clock_t>;
using Milliseconds_t = std::chrono::milliseconds;


class Timer
{
public:
	Timer();

	TimePoint_t reset();
	double elapsed() const;
	double elapsed(const TimePoint_t &timePoint) const;

private:
	TimePoint_t startPoint;
	// std::chrono::time_point<high_resolution_clock> - ������ �������.
};
