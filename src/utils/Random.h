#pragma once
#include <cstdint>
#include <cstdlib>
#include <ctime>


class Random
{
public:
	/**
	 * Return random number
	 * @param from - from number (inclusively)
	 * @param till - till number (inclusively)
	 */
	static int32_t get(int32_t from, int32_t till);
	
private:
	class nested // static initializer
	{
	public:
		nested()
		{
			// set seed value
			srand(static_cast<unsigned int>(time(0)));
		}
	};

	static nested nsi;
};
