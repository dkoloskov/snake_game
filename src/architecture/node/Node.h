#pragma once


// Linked list data structure.
// Note: simplified version, since we don't need to add new nodes in between, etc.
class Node
{
public:
	virtual ~Node();

	virtual void addChild(Node *node);
	virtual void setParent(Node *node);
	
protected:
	Node *child;
	Node *parent;
};
