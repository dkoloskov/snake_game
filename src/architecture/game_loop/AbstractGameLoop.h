#pragma once
#include "../../utils/Timer.h"


/**
 * Design pattern "Game Loop"
 * http://gameprogrammingpatterns.com/game-loop.html
 * I think it can be called sub-type of "Template Method" design pattern
 */
class AbstractGameLoop
{
public:
	/**
	 * @param loopTime - game loop duration. Usually milliseconds per frame
	 */
	AbstractGameLoop(double loopTime);
	~AbstractGameLoop();

	// setup updateList, renderList, etc
	virtual void setup() = 0;
	//
	virtual void startGameLoop();
	void stopGameLoop(); // non virtual since we call it from destructor

protected:
	double loopTime; // in milliseconds
	bool loopEnabled = false;

	Timer timer;
	
	virtual void gameLoop();

	//
	virtual void processInput() = 0;
	virtual void update(double delta) = 0;
	virtual void render() = 0;
};
