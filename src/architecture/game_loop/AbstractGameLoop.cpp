#include "AbstractGameLoop.h"
#include <thread>


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
AbstractGameLoop::AbstractGameLoop(double loopTime)
{
	this->loopTime = loopTime;
}

void AbstractGameLoop::startGameLoop()
{
	loopEnabled = true;
	gameLoop();
}

void AbstractGameLoop::stopGameLoop()
{
	loopEnabled = false;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// TODO::Improvements:
//	- Handle lags (when actual gameLoop() time > loopTime)
void AbstractGameLoop::gameLoop()
{
	int32_t sleepTime;
	while (loopEnabled)
	{
		timer.reset();

		//
		processInput();
		update(loopTime);
		render();

		//
		sleepTime = loopTime - timer.elapsed(); // in milliseconds
		sleepTime = (sleepTime < 0) ? 0 : sleepTime; // we don't need negative values here
		std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
	}
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
AbstractGameLoop::~AbstractGameLoop()
{
	stopGameLoop();
}
