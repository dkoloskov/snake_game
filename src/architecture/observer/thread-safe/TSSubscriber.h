#pragma once
#include "../AbstractSubscriber.h"
#include <mutex>


/**
 * Type-safe Subscriber
 */
class TSSubscriber : public AbstractSubscriber
{
public:
	virtual void handleMessageTS(std::string message, std::any data);
	
protected:
	std::mutex handleMessageMutex;
};
