#pragma once
#include "../Dispatcher.h"
#include <mutex>


/**
 * Thread-safe Dispatcher
 */
class TSDispatcher : public Dispatcher
{
public:
	virtual bool subscribe(std::string message, AbstractSubscriber *subscriber) override;
	virtual bool unsubscribe(std::string message, AbstractSubscriber *subscriber) override;

	virtual void sendMessage(std::string message, std::any data) override;

protected:
	std::mutex subscribersMutex;
};
