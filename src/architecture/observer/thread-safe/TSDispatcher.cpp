#include "TSDispatcher.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
bool TSDispatcher::subscribe(std::string message, AbstractSubscriber *subscriber)
{
	std::unique_lock<std::mutex> uLock(subscribersMutex);
	return Dispatcher::subscribe(message, subscriber);
}

bool TSDispatcher::unsubscribe(std::string message, AbstractSubscriber *subscriber)
{
	std::unique_lock<std::mutex> uLock(subscribersMutex);
	return Dispatcher::unsubscribe(message, subscriber);
}


// TODO: Optimization. Move "copying map" to another method: subscribe/unsubscribe or
// ... update on next tick)
// Optimizations required since if sendMessage will be called 60 times per second performance will be bad
void TSDispatcher::sendMessage(std::string message, std::any data)
{
	// Lock, to save state of subscribers
	std::unique_lock<std::mutex> uLock(subscribersMutex);
	auto got = subscribersMap.find(message);
	// If there is NO such message with subscribers
	if (got == subscribersMap.end())
		return;

	//
	std::unordered_set<AbstractSubscriber*> subscribersCopy = got->second;
	// We can unlock since we will work with copy of subscribers now
	// Note: this lock is required to avoid deadlock if subscribe()/unsubscribe() will be called from handleMessage()
	uLock.unlock();

	for (auto &it : subscribersCopy)
	{
		AbstractSubscriber *subscriber = it;
		// Check, just to be sure, that pointer wasn't cleared in another thread
		if (subscriber != nullptr) // 
		{
			subscriber->handleMessage(message, data);
		}
	}
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
