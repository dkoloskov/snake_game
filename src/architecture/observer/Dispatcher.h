#pragma once
#include <unordered_set>
#include <unordered_map>
#include <any>
#include "AbstractSubscriber.h"

// std::string message. Each message has array of own observers.
using SubscribersMap_t = std::unordered_map<std::string, std::unordered_set<AbstractSubscriber*>>;


/**
 * Also known as "Observable/IObservable"
 * Part of Observer design pattern
 */
class Dispatcher
{
public:
	virtual ~Dispatcher();

	virtual bool subscribe(std::string message, AbstractSubscriber *subscriber);
	virtual bool unsubscribe(std::string message, AbstractSubscriber *subscriber);

	//
	virtual void sendMessage(std::string message, std::any data = nullptr);

protected:
	SubscribersMap_t subscribersMap;
};
