#include "Dispatcher.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
bool Dispatcher::subscribe(std::string message, AbstractSubscriber *subscriber)
{
	auto got = subscribersMap.find(message);
	// Check if map doesn't have array for current message yet
	if (got == subscribersMap.end())
	{
		subscribersMap.insert({message, {subscriber}});
		return true;
	}

	// got->second - set of subscribers
	// Insert subscriber to set AND return result (.second)
	return got->second.insert(subscriber).second;
}

bool Dispatcher::unsubscribe(std::string message, AbstractSubscriber *subscriber)
{
	auto got = subscribersMap.find(message);
	// Check if map doesn't have array for current message
	if (got == subscribersMap.end())
	{
		return false;
	}

	return got->second.erase(subscriber);
	// TODO: also need to erase map pair, if subscribers set is empty
}

//
void Dispatcher::sendMessage(std::string message, std::any data)
{
	auto got = subscribersMap.find(message);
	// If there is NO such message with subscribers
	if (got == subscribersMap.end())
		return;

	for (auto &it : got->second)
	{
		AbstractSubscriber *subscriber = it;
		subscriber->handleMessage(message, data);
	}
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
Dispatcher::~Dispatcher()
{
	subscribersMap.clear();
}