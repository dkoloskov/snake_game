#pragma once
#include <cstdint>
#include <iostream>
#include <any>


/**
 * Also known as "Observer/IObserver"
 * * Part of Observer design pattern
 */
class AbstractSubscriber
{
public:
	AbstractSubscriber();
	
	virtual ~AbstractSubscriber() {};

	virtual void handleMessage(std::string message, std::any data) = 0;

	uint32_t getSubscriberId();

private:
	uint32_t subscriberId;

	static uint32_t idCounter;
};